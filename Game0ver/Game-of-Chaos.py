#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Game of Chaos"""


import random
from matplotlib import pyplot as plt


A = [0, 0]
B = [1, 0]
C = [0.5, 1]

x0 = random.uniform(0, 1)
y0 = random.uniform(0, 1)

x = []
y = []

plt.figure(figsize=(12, 7))

for i in range(10000):
    V = random.choice([A, B, C])
    x.append(x0 + (V[0] - x0) / 2)
    y.append(y0 + (V[1] - y0) / 2)
    x0 = x[-1]
    y0 = y[-1]

    plt.clf()
    plt.title("Game of Chaos")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.plot(A[0], A[1], "b,")
    plt.plot(B[0], B[1], "b,")
    plt.plot(C[0], C[1], "b,")
    plt.plot(x, y, ".")
    plt.grid(False)
    plt.savefig("game" + str(i) + ".png")
    print(".", end="", flush=True)
