#!/bin/bash
#PBS -N OPERA
#PBS -l walltime=24h
#PBS -l nodes=1:ppn=1:x86_64:brno
#PBS -l mem=4gb
#PBS -l scratch=4gb
#PBS -m abe
#PBS -M janak@physics.muni.cz
trap 'clean_scratch' TERM EXIT

module add python-2.7.6-gcc

KRNO=/storage/brno6/home/janak
OPERA=$KRNO/OPERA/
NIGHT=2013-07-04

cd $OPERA

cp cfitsio*.tar.gz $SCRATCHDIR
cp fftw-3.3.4.tar.gz $SCRATCHDIR
cp opera-1.0_OES.zip $SCRATCHDIR
cp -r data $SCRATCHDIR
cp -r xdata $SCRATCHDIR

cd $SCRATCHDIR
OPERA=`pwd`

tar xf fftw-3.3.4.tar.gz
cd fftw-3.3.4
./configure --prefix=$OPERA/opera-1.0/
make
cd ..

for c in cfitsio*.tar.gz
do
    C=${c%.tar.gz}
    unzip opera-1.0_OES.zip
    tar xf $c
    cd fftw-3.3.4
    make install
    cd ..
    cd cfitsio
    ./configure --prefix=$OPERA/opera-1.0/
    make > ../$C-build.log
    make install > ../$C-install.log
    cd ..
    cd opera-1.0
    ./configure --prefix=`pwd` \
                --with-cfitsioinc=include \
                --with-cfitsiolib=lib                
    make > ../opera-$C-build.log
    make install > ../opera-$C-install.log
    cd ..
    python2 ./opera-1.0/pipeline/OES/operaoes.py \
            --datarootdir=data \
            --pipelinehomedir=opera-1.0 \
            --productrootdir=xdata \
            --night=$NIGHT \
            --product="OBJECTS" -pvt \
            2> opera-$C-error.log \
            > opera-$C-output.log
    rm -rf xdata/$NIGHT
    rm -rf opera-1.0
    rm -rf __MACOSX
done

cp *.log $KRNO/OPERA/log/ || export CLEAN_SCRATCH=false
